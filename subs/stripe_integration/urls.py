from django.urls import path
from subs.stripe_integration.api.views import create_subscription_view, \
    list_payment_methods_view, \
    cancel_subscription_view, \
    list_subscription_view, \
    webhook_received

app_name = "subscription"

urlpatterns = [
    path("v1/create-subscription/", create_subscription_view),
    path("v1/list-payment-methods/<str:type>", list_payment_methods_view),
    path("v1/cancel-subscription/<id>", cancel_subscription_view),
    path("v1/list-subscriptions/", list_subscription_view),
    path("v1/webhook/", webhook_received),

    # APIs that can be implemented more
    # path("v1/update-subscription/", ),
    # path("v1/list-invoices/",),
    # path("v1/list-payments/",),

]
