from datetime import datetime


def SubscriptionSerializer(subscription):
    """
    this function is used to serialize the data from subscription
    object. we can add more field as per the requirement.
    :param subscription: object
    :return: hash of objects' fields
    """
    data = dict(sub_id=subscription.id, status=subscription.status,
                start_date=datetime.utcfromtimestamp(subscription.start_date)
                .strftime('%Y-%m-%dT%H:%M:%SZ')
                if subscription.start_date is not None else None,
                trial_end=datetime.utcfromtimestamp(subscription.trial_end)
                .strftime('%Y-%m-%dT%H:%M:%SZ')
                if subscription.trial_end is not None else None,
                trial_start=datetime.utcfromtimestamp(subscription.trial_start)
                .strftime('%Y-%m-%dT%H:%M:%SZ')
                if subscription.trial_start is not None else None,
                canceled_at=datetime.utcfromtimestamp(subscription.canceled_at)
                .strftime('%Y-%m-%dT%H:%M:%SZ')
                if subscription.canceled_at is not None else None,
                created=datetime.utcfromtimestamp(subscription.created)
                .strftime('%Y-%m-%dT%H:%M:%SZ')
                if subscription.created is not None else None)
    return data


def SubscriptionListSerializer(subscriptions):
    """
    this function is used to serialize the list data from subscription
    object. we can add more field as per the requirement.
    :param subscriptions: object
    :return: list of hash of objects fields
    """
    data = subscriptions.data
    lst = []
    for x in data:
        json_data = SubscriptionSerializer(x)
        lst.append(json_data)
    return lst


def PaymentMethodSerializer(instance):
    """
    this function is used to serialize the data from payment
    method object. we can add more field as per the requirement.
    :param instance: object
    :return: hash of objects' fields
    """
    data = instance.data
    lst = []
    for x in data:
        json_data = {"id": x.id,
                     "exp_month": x.card.exp_month,
                     "exp_year": x.card.exp_year,
                     "last4": x.card.last4,
                     "type": x.type,
                     "brand": x.card.brand}

        lst.append(json_data)
    return lst
