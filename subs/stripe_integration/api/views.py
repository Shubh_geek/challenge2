import json
import os

from requests import Response
from rest_framework.response import *
from rest_framework.decorators import api_view
from rest_framework import status

from subs.stripe_integration.api.serializers import SubscriptionSerializer, \
    PaymentMethodSerializer, SubscriptionListSerializer
from subs.stripe_integration.utils import create_stripe_customer, \
    get_stripe_object, handle_exception


@api_view(['POST'])
def cancel_subscription_view(request, id):
    if request.method == 'POST':
        try:
            deleted_subs = get_stripe_object().Subscription.delete(id)
            serializer = SubscriptionSerializer(deleted_subs)
            return Response(data=serializer, status=status.HTTP_201_CREATED)
        except Exception as e:
            return handle_exception(e)


@api_view(['GET'])
def list_subscription_view(request):
    if request.method == 'GET':
        user = request.user
        sub_status = request.GET.get("status") \
            if "status" in request.GET \
            else None
        try:
            if sub_status:
                instance = get_stripe_object().Subscription.list(
                    customer=user.stripe_cust_id,
                    status=sub_status
                )
            else:
                instance = get_stripe_object().Subscription.list(
                    customer=user.stripe_cust_id,
                )
            data = SubscriptionListSerializer(instance)
            return Response(data=data, status=status.HTTP_200_OK)
        except Exception as e:
            return handle_exception(e)


@api_view(['GET'])
def list_payment_methods_view(request, type):
    if request.method == 'GET':
        user = request.user
        try:
            instance = get_stripe_object().PaymentMethod.list(
                customer=user.stripe_cust_id,
                type=type,
            )
            data = PaymentMethodSerializer(instance)
            return Response(data=data, status=status.HTTP_200_OK)
        except Exception as e:
            return handle_exception(e)


@api_view(['POST'])
def create_subscription_view(request):
    if request.method == 'POST':
        user = request.user
        data = request.data
        # Check if stripe customer exists. if not, create one to proceed.
        if user.stripe_cust_id is None:
            try:
                customer = create_stripe_customer(user.email, user.name)
                user.stripe_cust_id = customer.id
                user.save()
            except Exception as e:
                return handle_exception(e)
        customer_id = user.stripe_cust_id
        try:
            """
            Check if user has selected the payment method from previous
            methods
            """
            if "paymentId" in data:
                payment_method = get_stripe_object().PaymentMethod.\
                    retrieve(data["paymentId"])
            else:
                # Create a new payment method
                payment_method = get_stripe_object().PaymentMethod.create(
                    type=data["type"],
                    card={
                        "number": data["number"],
                        "exp_month": data["exp_month"],
                        "exp_year": data["exp_year"],
                        "cvc": data["cvc"],
                    },
                )
                payment_method = get_stripe_object().PaymentMethod.attach(
                    payment_method.id,
                    customer=customer_id
                    )

            # Create the subscription if test period is selected.
            if "trial" in data:
                subscription = get_stripe_object().Subscription.create(
                    trial_period_days=data["trial"],
                    default_payment_method=payment_method.id,
                    customer=customer_id,
                    items=[{
                        'price': os.getenv(data['price'])
                    }],
                )
            else:
                subscription = get_stripe_object().Subscription.create(
                    default_payment_method=payment_method.id,
                    customer=customer_id,
                    items=[{
                        'price': os.getenv(data['price'])
                    }],
                )
            serializer = SubscriptionSerializer(subscription)
            return Response(data=serializer, status=status.HTTP_201_CREATED)
        except Exception as e:
            return handle_exception(e)


@api_view(['POST'])
def webhook_received(request):
    # Webhook to receive information about different payment failures/success,
    # and other various events.
    webhook_secret = os.getenv('STRIPE_WEBHOOK_SECRET')
    request_data = request.data

    if webhook_secret:
        signature = request.headers.get('stripe-signature')
        try:
            event = get_stripe_object().Webhook.construct_event(
                payload=request_data,
                sig_header=signature,
                secret=webhook_secret
            )
            data = event['data']
        except Exception as e:
            return handle_exception(e)
        event_type = event['type']
    else:
        data = request_data['data']
        event_type = request_data['type']

    if event_type == 'invoice.payment_action_required':
        """
        Used to notify the user that an authentication action is required by
        the user to complete the payment.
        """
        # print(data)
        pass

    elif event_type == 'invoice.payment_failed':
        """
        Used to notify the user that payment has failed so new card details
        needs to be entered and attached. We can update our local DB's status
        etc as well to stop access of the service.
        """
        # print(data)
        pass

    return Response(data={'status': 'success'})
