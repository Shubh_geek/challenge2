import stripe
import os

from rest_framework import status
from rest_framework.response import Response

stripe.api_key = os.getenv('STRIPE_SECRET_KEY')
stripe.api_version = os.getenv('STRIPE_API_VERSION')


def get_stripe_object():
    """
    It returns a Strip object.
    :return: Stripe object
    """
    return stripe


def create_stripe_customer(email, name):
    """
    This util method is created to create a new Stripe Customer.
    :param email: email of the user (optional)
    :param name: name of the customer (optional)
    :return: stripe customer object or Response object.
    """
    try:
        customer = stripe.Customer.create(email=email, name=name)
    except Exception as e:
        return handle_exception(e)
    return customer


def handle_exception(e):
    """
    A generic function to capture exception, formulate a message and
    send to admin/dev team
    :param e: exception object (required)
    :return: Response object
    """
    if e.http_status and str(e.http_status).startswith("4"):
        # send email to dev/admin team with the following message
        msg = "The request ({}) failed with an error status {} with error " \
              "message ({}) and code ({}). Please check."\
            .format(e.request_id, e.http_status, e.user_message, e.code)
    elif e.http_status and str(e.http_status).startswith("5"):
        msg = "Stripe server is not reachable. Please check."

    return Response(data={"message": "There is some error in processing your "
                                     "request. Please wait for sometime."},
                    status=status.HTTP_400_BAD_REQUEST)
