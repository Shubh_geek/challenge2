from django.db.models.signals import post_save
from subs.users.models import User
from subs.stripe_integration.utils import get_stripe_object, handle_exception


def create_stripe_customer(sender, instance, created, update_fields,
                           *args, **kwargs):
    if instance:
        try:
            if instance.stripe_cust_id is None:
                # Creating stripe customer upon successful
                # registration with dummy data.
                customer = get_stripe_object().Customer. \
                    create(email=instance.email,
                           name=instance.name,
                           address={
                               "line1": "Street1",
                               "line2": "",
                               "city": "Delhi",
                               "state": "DL",
                               "postal_code": "110016",
                               "country": "IN"
                           },
                           shipping={
                               "phone": "",
                               "name": "Shubh",
                               "address": {
                                   "line1": "Street1",
                                   "line2": "",
                                   "city": "Delhi",
                                   "state": "DL",
                                   "postal_code": "110016",
                                   "country": "IN"
                               }
                           }
                           )
                instance.stripe_cust_id = customer.id
                instance.save()
        except Exception as e:
            # Send an email to Admin/dev team
            if str(e.http_status).startswith("4"):
                # send email to dev/admin team with the following message
                msg = "The request ({}) failed with an error status {} with " \
                      "error message ({}) and code ({}). " \
                      "Please check.".format(e.request_id,
                                             e.http_status,
                                             e.user_message,
                                             e.code)
            elif str(e.http_status).startswith("5"):
                msg = "Stripe server is not reachable. Please check."
            # send email with msg and subject.


post_save.connect(create_stripe_customer, sender=User)
