from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.db import models

from subs.stripe_integration.utils import get_stripe_object


class User(AbstractUser):
    """Default user for Subscription."""

    #: First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"),
                     blank=True,
                     null=True,
                     max_length=255
                     )
    password = models.TextField(_("Password"))
    first_name = None
    last_name = None
    date_joined = None
    email = models.EmailField(_('email address'),
                              blank=None,
                              null=None,
                              unique=True
                              )
    stripe_cust_id = models.CharField(max_length=256, blank=True, default=None)

    @staticmethod
    def get_email_field_name():
        return "email"

    @staticmethod
    def get_name_field():
        return "name"

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})

